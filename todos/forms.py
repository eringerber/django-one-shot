from django.db.models.base import Model
from django.forms import ModelForm
from django.forms import ModelChoiceField
from todos.models import TodoList, TodoItem


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class TodoListChoiceField(ModelChoiceField):
    def label_from_instance(self, obj: Model) -> str:
        return obj.name


class TodoItemForm(ModelForm):
    list = TodoListChoiceField(queryset=TodoList.objects.all(), label="list")

    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
